﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ICEImport.Helpers
{
    public static class StringExtensions
    {
        public static bool IsNullEmptyOrWhiteSpace(this String s)
        {
            return string.IsNullOrWhiteSpace(s);
        }
        public static T CastTo<T>(this String s)
        {
            try
            {
                return s.IsNullEmptyOrWhiteSpace() ? default(T) : (T)Convert.ChangeType(s, typeof(T));
            }
            catch (FormatException)
            {
                return s.GetDefaultValue<T>();
            }
        }
    }
}
