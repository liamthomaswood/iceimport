﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ICEImport.Helpers
{
    public static class ObjectExtensions
    {
        public static bool IsNull(this object o)
        {
            return o == null;
        }
        public static bool IsNotNull(this object o)
        {
            return o != null;
        }
        public static T GetDefaultValue<T>(this object o)
        {
            Type objectType = o.GetType();

            if (objectType == typeof(String))
            {
                return (T)(object)String.Empty;
            }

            return default(T);
        }
    }
}
