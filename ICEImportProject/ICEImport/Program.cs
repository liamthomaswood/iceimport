﻿using ICEImport.Configuration;
using ICEImport.Ninject;
using ICEImport.Services;
using ICEImport.SQLConnections;
using ICEImport.TableGateways;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ICEImport
{
    class Program
    {
        static IMarketInfoPriceService _marketInfoPriceService;
        static IKernel _ninjectKernel;
        static IApplicationSettings _applicationSettings;
        static IMarketInfoPriceTableGateway _marketInfoPriceTableGateway;
        static ISqlConnectionStringManager _sqlConnectionManager;

        static void Main(string[] args)
        {
            InitialiseDependencies();
            Console.WriteLine(_marketInfoPriceTableGateway.DatabaseName);
            var latestMarketInfoPriceDates
                = _marketInfoPriceService.GetLatestMarketInfoPriceDatesDto();

            Console.WriteLine(latestMarketInfoPriceDates.ElectricityPrice);
        }

        public static void InitialiseDependencies()
        {
            _ninjectKernel
                = NinjectFactory
                    .GetInitialisedStandardKernelUsing(new NinjectCommon().RegisterServices);

            _applicationSettings = _ninjectKernel.Get<IApplicationSettings>();
            _marketInfoPriceTableGateway = _ninjectKernel.Get<IMarketInfoPriceTableGateway>();
            _sqlConnectionManager = _ninjectKernel.Get<ISqlConnectionStringManager>();
            _marketInfoPriceService = _ninjectKernel.Get<IMarketInfoPriceService>();


        }
    }
}
