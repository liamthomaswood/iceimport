﻿using System;

namespace ICEImport.Dto
{
    public class LatestMarketInfoPriceDto
    {
        public DateTime ElectricDate { get; set; }
        public DateTime GasDate { get; set; }
        public float ElectricityPrice { get; set; }
        public float GasPrice { get; set; }
    }
}