﻿using ICEImport.Helpers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ICEImport.Factories
{
    public class EntityDataTableFactory : IEntityDataTableFactory
    {
        public DataTable CreateDataTableFrom<T>(List<T> entityList) where T : class
        {
            if (entityList.IsNullOrEmpty())
                return new DataTable();

            DataTable entityDataTable = CreateDataTableFor(entityList);

            const int MinimumExpectedColumns = 1;
            if (entityDataTable.Columns.Count < MinimumExpectedColumns)
                return new DataTable();

            foreach (var entity in entityList)
            {
                GenerateDataRowFrom(entity, ref entityDataTable);
            }

            return entityDataTable;
        }

        private DataTable CreateDataTableFor<T>(List<T> entityList) where T : class
        {
            Type classType = entityList.First().GetType();

            List<PropertyInfo> propertyList = classType.GetProperties().ToList();

            const int MinimumPropertyCount = 1;
            if (propertyList.Count < MinimumPropertyCount)
            {
                return new DataTable();
            }

            string entityName = classType.UnderlyingSystemType.Name;
            DataTable entityDataTable = new DataTable(entityName);

            foreach(PropertyInfo property in propertyList)
            {
                DataColumn column = new DataColumn();
                column.ColumnName = property.Name;

                Type dataType = property.PropertyType;

                if (IsNullable(dataType))
                {
                    if (dataType.IsGenericType)
                    {
                        dataType = dataType.GenericTypeArguments.FirstOrDefault();
                    }
                }
                else
                {
                    column.AllowDBNull = false;
                }

                column.DataType = dataType;

                entityDataTable.Columns.Add(column);
            }

            return entityDataTable;
        }

        private void GenerateDataRowFrom<T>(T entity, ref DataTable entityDataTable) where T : class
        {
            Type classType = entity.GetType();

            DataRow row = entityDataTable.NewRow();
            List<PropertyInfo> entityPropertyInfoList = classType.GetProperties().ToList();

            foreach(PropertyInfo propertyInfo in entityPropertyInfoList)
            {
                if (entityDataTable.Columns.Contains(propertyInfo.Name))
                {
                    if (entityDataTable.Columns[propertyInfo.Name].IsNotNull())
                    {
                        row[propertyInfo.Name] = propertyInfo.GetValue(entity, null) ?? DBNull.Value;
                    }
                }
            }

            entityDataTable.Rows.Add(row);
        }

        private bool IsNullable(Type type)
        {
            if (type.IsValueType.IsFalse())
            {
                return true;
            }

            if (Nullable.GetUnderlyingType(type).IsNotNull())
            {
                return true;
            }

            return false;
        }
    }
}
