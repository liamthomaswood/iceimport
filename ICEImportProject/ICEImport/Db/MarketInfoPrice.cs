﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ICEImport.Db
{
    public class MarketInfoPrice
    {
        [Key, Column(Order = 1)]
        public bool Electricity { get; set; }

        [Key, Column(Order = 2)]
        public DateTime FutureDate { get; set; }

        [Key, Column(Order = 3)]
        public DateTime Month { get; set; }

        public float Price { get; set; }
    }
}
