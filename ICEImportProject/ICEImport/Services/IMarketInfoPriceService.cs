﻿using ICEImport.Db;
//using ICEImport.TableGateways;
using ICEImport.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ICEImport.Services
{
    public interface IMarketInfoPriceService
    {
        LatestMarketInfoPriceDto GetLatestMarketInfoPriceDatesDto();
        void Import(List<MarketInfoPrice> marketInfoPriceList);
    }
}
