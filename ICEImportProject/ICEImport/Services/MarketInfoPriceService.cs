﻿using ICEImport.Db;
using ICEImport.Factories;
using ICEImport.Logging;
using ICEImport.TableGateways;
using ICEImport.Dto;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ICEImport.Services
{
    public class MarketInfoPriceService : IMarketInfoPriceService
    {
        readonly IMarketInfoPriceTableGateway _marketInfoPriceTableGateway;
        readonly IEntityDataTableFactory _entityDataTableFactory;
        //readonly IStreamWriter _consoleWriter;

        public MarketInfoPriceService(
                IMarketInfoPriceTableGateway marketInfoPriceTableGateway,
                IEntityDataTableFactory entityDataTableFactory)
        {
            _marketInfoPriceTableGateway = marketInfoPriceTableGateway;
            _entityDataTableFactory = entityDataTableFactory;
        }
        public LatestMarketInfoPriceDto GetLatestMarketInfoPriceDatesDto()
        {
            LatestMarketInfoPriceDto latestMarketDatesDto
                = new LatestMarketInfoPriceDto();

            latestMarketDatesDto = _marketInfoPriceTableGateway.GetLatestMarketDataDates();

            return latestMarketDatesDto;
        }

        public void Import(List<MarketInfoPrice> marketInfoPriceList)
        {
            DataTable marketInfoPriceTable = _entityDataTableFactory.CreateDataTableFrom(marketInfoPriceList);
            _marketInfoPriceTableGateway.Import(marketInfoPriceTable);
        }
    }
}
