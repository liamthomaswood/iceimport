﻿using ICEImport.Configuration;
using ICEImport.Factories;
using ICEImport.Services;
using ICEImport.SQLConnections;
using Ninject;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ICEImport.Ninject
{
    public static class NinjectFactory
    {
        public static IKernel GetInitialisedStandardKernelUsing(Action<IKernel> bindingMethod)
        {
            IKernel kernel = new StandardKernel();
            RegisterCommonDependencies(kernel);
            bindingMethod.Invoke(kernel);
            return kernel;
        }

        public static void RegisterCommonDependencies(IKernel kernel)
        {
            kernel.Bind<IEntityDataTableFactory>().To<EntityDataTableFactory>().InTransientScope();
            kernel.Bind<IConnectionStringManager>().To<WebConfigConnectionManager>().InTransientScope();
            //kernel.Bind<IApplicationSettings>().To<ApplicationSettings>().InTransientScope();
            kernel.Bind<IConfigurationManager>().To<WebConfigConfigurationManager>().InTransientScope();
            kernel.Bind<IApplicationSettings>().To<ApplicationSettings>().InTransientScope();
            kernel.Bind<IConnectionSettings>().To<Configuration.ConnectionStringSettings>().InTransientScope();
            kernel.Bind<ISqlConnectionStringManager>().To<SqlConnectionStringManager>().InTransientScope();
            kernel.Bind<IMarketInfoPriceService>().To<MarketInfoPriceService>().InTransientScope();
        }
    }
}
