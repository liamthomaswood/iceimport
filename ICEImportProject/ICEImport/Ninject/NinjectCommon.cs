﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ninject;

using ICEImport.TableGateways;

namespace ICEImport.Ninject
{
    public class NinjectCommon
    {
        public void RegisterServices(IKernel kernel)
        {
            kernel.Bind<IMarketInfoPriceTableGateway>().To<MarketInfoPriceTableGateway>().InTransientScope();
        }
    }
}
