﻿using ICEImport.Configuration;
using ICEImport.Helpers;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ICEImport.SQLConnections
{
    public class SqlConnectionStringManager: ISqlConnectionStringManager
    {
        readonly IApplicationSettings _applicationSettings;
        readonly IConnectionSettings _connectionSettings;

        public SqlConnectionStringManager(IApplicationSettings applicationSettings, IConnectionSettings connectionSettings)
        {
            _applicationSettings = applicationSettings;
            _connectionSettings = connectionSettings;
        }


        public string GetConnectionStringWith(string connectionName)
        {
            if (connectionName.IsNullEmptyOrWhiteSpace())
                throw new ArgumentNullException
                (
                    "connectionString",
                    "connectionString is null or empty"
                );

            return _connectionSettings.GetConnectionStringBy(connectionName);
        }

        public string GetDatabaseNameFor(string connectionName)
        {
            string connectionString =
                _connectionSettings.GetConnectionStringBy(connectionName);

            return new SqlConnectionStringBuilder(connectionString).InitialCatalog;
        }
    }
}
