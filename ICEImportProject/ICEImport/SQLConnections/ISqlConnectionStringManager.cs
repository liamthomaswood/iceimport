﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ICEImport.SQLConnections
{
    public interface ISqlConnectionStringManager
    {
        string GetConnectionStringWith(string connectionName);
        string GetDatabaseNameFor(string connectionName);
    }
}
