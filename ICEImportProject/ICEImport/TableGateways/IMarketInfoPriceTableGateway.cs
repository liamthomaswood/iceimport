﻿using ICEImport.Dto;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ICEImport.TableGateways
{
    public interface IMarketInfoPriceTableGateway
    {
        string DatabaseName { get; }
        LatestMarketInfoPriceDto GetLatestMarketDataDates();
        void Import(DataTable dataTable);
    }
}
