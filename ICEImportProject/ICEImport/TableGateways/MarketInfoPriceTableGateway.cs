﻿using ICEImport.Helpers;
using ICEImport.SQLConnections;
using ICEImport.Dto;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ICEImport.TableGateways
{
    public class MarketInfoPriceTableGateway:IMarketInfoPriceTableGateway
    {
        readonly string _databaseName;
        readonly string _sqlConnectionString;

        const string MarketInfoPriceTableName = "MarketPriceInfo_ToBeDeleted";
        const string DefaultConnectionStringName = "DefaultConnection";

        public string DatabaseName { get { return _databaseName; } }

        public MarketInfoPriceTableGateway(ISqlConnectionStringManager sqlConnectionStringManager)
        {
            _databaseName =
                sqlConnectionStringManager.GetDatabaseNameFor(DefaultConnectionStringName);

            _sqlConnectionString
                = sqlConnectionStringManager.GetConnectionStringWith(DefaultConnectionStringName);
        }

        public LatestMarketInfoPriceDto GetLatestMarketDataDates()
        {
            DateTime latestElectricDateTime = new DateTime();
            DateTime latestGasDateTime = new DateTime();
            float latestElectricityPrice = 0;
            float latestGasPrice = 0;

            using (var sqlConnection = new System.Data.SqlClient.SqlConnection(_sqlConnectionString))
            {
                sqlConnection.Open();

                SqlDataReader reader
                    = GetLatestMarketDataDatesSqlCommandUsing(sqlConnection).ExecuteReader();

                if (reader.HasRows)
                {
                    const string IsElectricColumn = "IsElectric";
                    const string LatestDateColumn = "LatestDate";

                    while (reader.Read())
                    {
                        if (reader[IsElectricColumn].ToString().CastTo<bool>())
                        {
                            latestElectricDateTime = DateTime.Parse(reader[LatestDateColumn].ToString());
                            latestElectricityPrice = float.Parse(reader["MarketPrice"].ToString());
                        }
                        else
                        {
                            latestGasDateTime = DateTime.Parse(reader[LatestDateColumn].ToString());
                            latestGasPrice = float.Parse(reader["MarketPrice"].ToString());
                        }
                    }
                }
                reader.Close();
            }
            return new LatestMarketInfoPriceDto
            {
                ElectricDate = latestElectricDateTime,
                GasDate = latestGasDateTime,
                ElectricityPrice = latestElectricityPrice,
                GasPrice = latestGasPrice
            };
        }

        private SqlCommand GetLatestMarketDataDatesSqlCommandUsing(System.Data.SqlClient.SqlConnection sqlConnection)
        {
            const string SqlCommandSyntax
                = @"DECLARE @MarketInfoPricesDates TABLE
                    (
                      LatestDate DATE, 
                      IsElectric BIT,
                      MarketPrice FLOAT
                    )

                    INSERT INTO @MarketInfoPricesDates (LatestDate, IsElectric, MarketPrice)
                        SELECT	TOP(1)FutureDate AS LatestDate,
                                0 AS IsElectric, Price AS MarketPrice
                        FROM	MarketInfoPrice
                        WHERE	Electricity = 0
                        ORDER BY	FutureDate DESC,
                                    Month ASC

                    INSERT INTO @MarketInfoPricesDates (LatestDate, IsElectric, MarketPrice)
                        SELECT	TOP(1)FutureDate AS LatestDate,
                                1 AS IsElectric, Price AS MarketPrice
                        FROM	MarketInfoPrice
                        WHERE	Electricity = 1
                        ORDER BY	FutureDate DESC,
                                    Month ASC
                    SELECT  *
                    FROM    @MarketInfoPricesDates";
            return new SqlCommand(SqlCommandSyntax, sqlConnection);
        }

        public void Import(DataTable dataTable)
        {
            if (dataTable.IsNull())
            {
                const string NullDataTableMessage = "entity is null";
                throw new ArgumentNullException(NullDataTableMessage);
            }
            try
            {
                using (System.Data.SqlClient.SqlConnection connection
                    = new System.Data.SqlClient.SqlConnection(_sqlConnectionString))
                {
                    connection.Open();
                    using(SqlTransaction transaction = connection.BeginTransaction())
                    {
                        using (SqlBulkCopy bulkCopyLoader =
                            new SqlBulkCopy(connection, SqlBulkCopyOptions.TableLock | SqlBulkCopyOptions.FireTriggers, transaction))
                        {
                            bulkCopyLoader.DestinationTableName = MarketInfoPriceTableName;
                            bulkCopyLoader.WriteToServer(dataTable);
                        }
                        transaction.Commit();
                    }
                }
            }
            catch
            {
                throw;
            }
        }
    }
}
