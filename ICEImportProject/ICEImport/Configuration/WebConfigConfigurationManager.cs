﻿using System.Collections.Specialized;
using System.Configuration;

namespace ICEImport.Configuration
{
    public class WebConfigConfigurationManager : IConfigurationManager
    {
        public virtual NameValueCollection AppSettings => ConfigurationManager.AppSettings;
        public void ClearCachedSettings()
        {
            return;
        }
    }
}
