﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ICEImport.Configuration
{
    public class WebConfigConnectionManager : IConnectionStringManager
    {
        public virtual ConnectionStringSettingsCollection ConnectionStrings
            => ConfigurationManager.ConnectionStrings;

        public void ClearCachedSettings()
        {
            return;
        }
    }
}
