﻿using ICEImport.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ICEImport.Configuration
{
    public class ApplicationSettings : IApplicationSettings
    {
        readonly IConfigurationManager _configurationManager;

        public ApplicationSettings(IConfigurationManager configurationManager)
        {
            _configurationManager = configurationManager;
        }

        public T Get<T>(string applicationSettingKey)
        {
            if (applicationSettingKey.IsNullEmptyOrWhiteSpace())
            {
                const string MissingParameter = "applicationSettingKey";
                throw GetMissingApplicationSettingExceptionFor(MissingParameter);
            }
            try
            {
                var applicationSettingValue
                    = _configurationManager.AppSettings[applicationSettingKey];

                if (applicationSettingValue.IsNull())
                    throw new Exception();

                var converter = TypeDescriptor.GetConverter(typeof(T));
                return (T)(converter.ConvertFromInvariantString(applicationSettingValue));
            }
            catch
            {
                throw;
            }
        }


        private ArgumentNullException GetMissingApplicationSettingExceptionFor(string parameterName)
        {
            const string MissingParameterMessage
                = "The {0} parameter is null, Empty or contains only whitespace characters";
            return new ArgumentNullException(parameterName,
                                             string.Format(MissingParameterMessage, parameterName));
        }

        public bool SettingExistsWith(string applicationSettingKey)
        {
            if (applicationSettingKey.IsNullEmptyOrWhiteSpace())
            {
                const string MissingParameter = "applicationSettingKey";
                throw GetMissingApplicationSettingExceptionFor(MissingParameter);
            }
            try
            {
                return _configurationManager.AppSettings[applicationSettingKey].IsNotNull();
            }
            catch
            {
                return false;
            }
        }

        public void ReloadCachedValues()
        {
            _configurationManager.ClearCachedSettings();
        }

        public T GetIfExists<T>(string applicationSettingKey)
        {
            if (!SettingExistsWith(applicationSettingKey))
                return typeof(T).Equals(typeof(string)) ? (T)(object)String.Empty : default(T);

            return Get<T>(applicationSettingKey);
        }
    }
}
