﻿using ICEImport.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ICEImport.Configuration
{
    class ConnectionStringSettings : IConnectionSettings
    {
        readonly IConnectionStringManager _connectionStringManager;
        public ConnectionStringSettings(IConnectionStringManager connectionStringManager)
        {
            _connectionStringManager = connectionStringManager;
        }

        public string GetConnectionStringBy(string connectionStringName)
        {
            if (connectionStringName.IsNullEmptyOrWhiteSpace())
            {
                const string MissingParameter = "connectionStringName";
                throw GetMissingApplicationSettingExceptionFor(MissingParameter);
            }

            try
            {
                var connectionString = _connectionStringManager
                                           .ConnectionStrings[connectionStringName]
                                           .ConnectionString;

                if (connectionString.IsNullEmptyOrWhiteSpace())
                    throw new Exception($"{connectionStringName}");

                return connectionString;
            }
            catch (Exception exception)
            {
                if (exception is NullReferenceException)
                {
                    throw new Exception($"{connectionStringName}");
                }
                else
                {
                    throw exception;
                }
            }
        }
        private ArgumentNullException GetMissingApplicationSettingExceptionFor(string parameterName)
        {
            const string MissingParameterMessage
                = "The {0} parameter is null, Empty or contains only whitespace characters";
            return new ArgumentNullException(parameterName,
                                             string.Format(MissingParameterMessage, parameterName));
        }
    }
}
