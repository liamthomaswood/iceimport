﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ICEImport.Configuration
{
    public interface IApplicationSettings
    {
        T Get<T>(string applicationSettingKey);
        bool SettingExistsWith(string applicationSettingKey);
        void ReloadCachedValues();
        T GetIfExists<T>(string applicationSettingKey);
    }
}
