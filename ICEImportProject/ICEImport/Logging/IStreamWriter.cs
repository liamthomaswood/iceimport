﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ICEImport.Logging
{
    public interface IStreamWriter
    {
        void WriteLine(string text, params object[] args);
        void WriteLine(bool addProgressSuffix, string text, params object[] args);

        void WriteLineWithNewLine(string text, params object[] args);
        void WriteToCurrentLine(string text, params object[] args);
        void WriteToCurrentLine(bool addProgressSuffix, string text, params object[] args);
        void WriteToCurrentLineAndFlush(string text, params object[] args);
        void WriteToCurrentLineAndFlush(bool addProgressSuffix, string text, params object[] args);
        void WriteCompletedSuffixToCurrentLineAndFlush();
        void WriteEmptyConsoleLine();
        void WriteExceptionToStreamUsing(Exception exception);


    }
}
